package com.alsnightsoft.examples.utils

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import groovy.transform.CompileStatic
import org.springframework.context.ConfigurableApplicationContext

/**
 *  Created by aluis on 5/11/16.
 */
@Singleton
@CompileStatic
class Constant {

    private final Gson gson = new GsonBuilder().create()
    public final String dataFileName = "Data.json"
    public ConfigurableApplicationContext applicationContext
    public static final String ROOT = "System"

    public String stringify(Object object) {
        return gson.toJson(object)
    }

    public final Object convert(String data, Class validClass) {
        if (data) {
            try {
                return gson.fromJson(data, validClass)
            } catch (Exception ex) {
                return null
            }
        }
        return null
    }

    public void setApplicationContext(ConfigurableApplicationContext applicationContext) {
        this.applicationContext = applicationContext
        autoWiredClass(this)
    }

    public void autoWiredClass(Object objectToWired) {
        applicationContext.getAutowireCapableBeanFactory().autowireBean(objectToWired)
    }
}
