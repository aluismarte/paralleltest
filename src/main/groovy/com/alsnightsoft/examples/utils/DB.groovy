package com.alsnightsoft.examples.utils

import com.alsnightsoft.examples.domains.GroupData
import com.alsnightsoft.examples.domains.NumbersData
import com.alsnightsoft.examples.models.constants.TypeData
import com.alsnightsoft.examples.services.GroupDataService
import com.alsnightsoft.examples.services.NumberDataService
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

import java.security.SecureRandom

/**
 *  Created by aluis on 5/12/16.
 */
@CompileStatic
class DB {

    @Autowired
    private GroupDataService groupDataService

    @Autowired
    private NumberDataService numberDataService

    private static final int GROUPS = 500
    private static final int DATA = 1000
    private final SecureRandom secureRandom = new SecureRandom()

    public DB() {
        Constant.instance.autoWiredClass(this)
    }

    public final void initDB() {
        if (groupDataService.countGroupData(true) == 0) {
            createData(TypeData.BASE)
            createData(TypeData.TWO_DIGITS)
        }
    }

    private void createData(int typeData) {
        println "Insert data " + typeData
        for (int i = 0; i < GROUPS; i++) {
            GroupData groupData = new GroupData(typeData: typeData)
            List<NumbersData> numbersDatas = new ArrayList<>()
            for (int j = 0; j < DATA; j++) {
                numbersDatas.add(numberDataService.createNumbersData(new NumbersData(data: generateData(typeData))))
            }
            groupData.setListNumbers(numbersDatas.toSet())
            groupDataService.createGroupData(groupData)
            println "%" + ((i / GROUPS) * 100)
        }
    }

    private String generateData(int typeData) {
        switch (typeData) {
            case TypeData.BASE:
                return secureRandom.nextInt(10) + "-" + secureRandom.nextInt(10) + "-" + secureRandom.nextInt(10)
            case TypeData.TWO_DIGITS:
                return secureRandom.nextInt(100) + "-" + secureRandom.nextInt(100) + "-" + secureRandom.nextInt(100)
        }
    }

}
