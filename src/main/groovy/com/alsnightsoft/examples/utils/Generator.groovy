package com.alsnightsoft.examples.utils

import com.alsnightsoft.examples.models.DataLoader
import com.alsnightsoft.examples.models.DataTest
import groovy.transform.CompileStatic

import java.security.SecureRandom

/**
 *  Created by aluis on 5/11/16.
 */
@CompileStatic
class Generator {

    private final SecureRandom secureRandom = new SecureRandom()

    public Generator() {
    }

    public final List<DataTest> generateRandomList(int size) {
        List<DataTest> dataTests = new ArrayList<>()
        for (int i = 0; i < size; i++) {
            dataTests.add(new DataTest(secureRandom.nextInt(10), secureRandom.nextInt(10), secureRandom.nextInt(10)))
        }
        return dataTests
    }

    public static final String convertData(List<DataTest> dataTests) {
        DataLoader dataLoader = new DataLoader()
        dataLoader.setDataTests(dataTests)
        return Constant.instance.stringify(dataLoader)
    }

    public final List<DataLoader> loadData() {
        List<DataLoader> dataLoaders = new ArrayList<>()
        File file = new File(getClass().getClassLoader().getResource(Constant.instance.dataFileName).getFile())
        for (String line : new BufferedReader(new FileReader(file)).readLines()) {
            dataLoaders.add((DataLoader) Constant.instance.convert(line, DataLoader.class))
        }
        return dataLoaders
    }
}
