package com.alsnightsoft.examples

import com.alsnightsoft.examples.utils.Constant
import com.alsnightsoft.examples.utils.DB
import groovy.transform.CompileStatic
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.transaction.annotation.EnableTransactionManagement

/**
 * On this test will check number repeats
 *
 *  Created by aluis on 5/11/16.
 */
@CompileStatic
@SpringBootApplication
@EnableTransactionManagement
class ParallelTestDBPump {

    public static void main(String[] args) throws Exception {
        dbTest(args)

        System.exit(0)
    }

    public static void dbTest(String[] args) {
        println "DB Test"
        Constant.instance.setApplicationContext(SpringApplication.run(ParallelTestDBPump.class, args))
        Constant.instance.autoWiredClass(this)
        new DB().initDB()
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize()
        Constant.instance.applicationContext.close()
    }

}
