package com.alsnightsoft.examples

import com.alsnightsoft.examples.tests.disk.*
import com.alsnightsoft.examples.utils.Constant
import groovy.transform.CompileStatic
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.transaction.annotation.EnableTransactionManagement

/**
 * On this test will check number repeats
 *
 *  Created by aluis on 5/11/16.
 */
@CompileStatic
@SpringBootApplication
@EnableTransactionManagement
class ParallelTestDisk {

    public static void main(String[] args) throws Exception {
        diskTest()
        System.exit(0)
    }

    public static void diskTest() {
        println "Disk Test"
        TestDiskMonoThread testMonoThread = new TestDiskMonoThread()
        testMonoThread.run()
        testMonoThread.shutdown()

        TestDiskMonoThreadSync testMonoThreadSync = new TestDiskMonoThreadSync()
        testMonoThreadSync.run()
        testMonoThreadSync.shutdown()

        TestDiskMultiThread testMultiThread = new TestDiskMultiThread()
        testMultiThread.run()
        testMultiThread.shutdown()

        TestDiskMultiThreadSync testMultiThreadSync = new TestDiskMultiThreadSync()
        testMultiThreadSync.run()
        testMultiThreadSync.shutdown()

        TestDiskMultiThreadWithBalance testMultiThreadWithBalance = new TestDiskMultiThreadWithBalance()
        testMultiThreadWithBalance.run()
        testMultiThreadWithBalance.shutdown()

        TestDiskMultiThreadWithBalanceSync testMultiThreadWithBalanceSync = new TestDiskMultiThreadWithBalanceSync()
        testMultiThreadWithBalanceSync.run()
        testMultiThreadWithBalanceSync.shutdown()
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize()
        Constant.instance.applicationContext.close()
    }

}
