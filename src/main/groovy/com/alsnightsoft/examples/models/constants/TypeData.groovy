package com.alsnightsoft.examples.models.constants

import groovy.transform.CompileStatic

/**
 *  Created by aluis on 5/12/16.
 */
@CompileStatic
class TypeData {
    public static final int BASE = 0
    public static final int TWO_DIGITS = 1
}