package com.alsnightsoft.examples.models.statistics

import groovy.transform.CompileStatic

/**
 *  Created by aluis on 5/11/16.
 */
@CompileStatic
class NumberFreq {

    String number
    long freq

    public NumberFreq(String number) {
        this.number = number
        this.freq = 0
    }

    public synchronized final void sumFreq() {
        freq++
    }
}
