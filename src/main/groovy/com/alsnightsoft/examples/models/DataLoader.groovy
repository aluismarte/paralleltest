package com.alsnightsoft.examples.models

import groovy.transform.CompileStatic

/**
 *  Created by aluis on 5/11/16.
 */
@CompileStatic
class DataLoader {

    List<DataTest> dataTests
}
