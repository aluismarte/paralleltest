package com.alsnightsoft.examples.models

import groovy.transform.CompileStatic

/**
 *  Created by aluis on 5/11/16.
 */
@CompileStatic
class DataTest {

    String data // 3 numbers  23-25-46

    public DataTest(int number1, int number2, int number3) {
        data = number1 + "-" + number2 + "-" + number3
    }
}
