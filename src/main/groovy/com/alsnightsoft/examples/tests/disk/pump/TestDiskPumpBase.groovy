package com.alsnightsoft.examples.tests.disk.pump

import com.alsnightsoft.examples.tests.Test
import groovy.transform.CompileStatic

/**
 *  Created by aluis on 5/12/16.
 */
@CompileStatic
abstract class TestDiskPumpBase extends Test {
}
