package com.alsnightsoft.examples.tests.disk

import com.alsnightsoft.examples.models.DataLoader
import com.alsnightsoft.examples.models.DataTest
import com.alsnightsoft.examples.models.statistics.NumberFreq
import com.alsnightsoft.examples.tests.Test
import com.alsnightsoft.examples.utils.Generator
import groovy.transform.CompileStatic

import java.util.concurrent.Callable

/**
 *  Created by aluis on 5/11/16.
 */
@CompileStatic
abstract class TestDiskBase extends Test {

    protected static final Generator generator = new Generator()
    protected static final List<DataLoader> data = generator.loadData()

    public void process(DataTest dataTest) {
        for (String split : dataTest.getData().split("-")) {
            sumFreq(split)
        }
    }

    public void process2(DataTest dataTest) {
        for (String split : dataTest.getData().split("-")) {
            sumFreq2(split)
        }
    }

    private void sumFreq(String number) {
        NumberFreq numberFreq = numberFreqs.get(Long.parseLong(number))
        if (!numberFreq) {
            synchronized (numberFreqs) {
                numberFreqs.put(Long.parseLong(number), new NumberFreq(number))
                numberFreq = numberFreqs.get(Long.parseLong(number))
            }
        }
        numberFreq.sumFreq()
    }

    private void sumFreq2(String number) {
        synchronized (numberFreqs) {
            NumberFreq numberFreq = numberFreqs.get(Long.parseLong(number))
            if (!numberFreq) {
                numberFreqs.put(Long.parseLong(number), new NumberFreq(number))
                numberFreq = numberFreqs.get(Long.parseLong(number))
            }
            numberFreq.sumFreq()
        }
    }

    protected void addThread(DataLoader dataLoader) {
        futures.add(processData(new Callable<Boolean>() {
            @Override
            Boolean call() throws Exception {
                for (DataTest dataTest : dataLoader.getDataTests()) {
                    process(dataTest)
                }
                return true
            }
        }))
    }

    protected void addThread2(DataLoader dataLoader) {
        futures.add(processData(new Callable<Boolean>() {
            @Override
            Boolean call() throws Exception {
                for (DataTest dataTest : dataLoader.getDataTests()) {
                    process2(dataTest)
                }
                return true
            }
        }))
    }
}
