package com.alsnightsoft.examples.tests.disk

import com.alsnightsoft.examples.models.DataLoader
import com.alsnightsoft.examples.models.DataTest
import groovy.transform.CompileStatic

import java.util.concurrent.Callable
import java.util.concurrent.Executors
import java.util.concurrent.Future

/**
 * Case mono thread
 *
 *  Created by aluis on 5/11/16.
 */
@CompileStatic
class TestDiskMonoThread extends TestDiskBase {

    public TestDiskMonoThread() {
        executorService = Executors.newFixedThreadPool(1)
    }

    @Override
    public final void run() {
        startTime()
        Future<Boolean> future = processData(new Callable<Boolean>() {
            @Override
            Boolean call() throws Exception {
                for (DataLoader dataLoader : data) {
                    for (DataTest dataTest : dataLoader.getDataTests()) {
                        process(dataTest)
                    }
                }
                return true
            }
        })
        future.get() // Force finish
        println "Test one thread only necessary sync"
        showFinishTime()
        // Show data results
        showStatistics()
    }
}
