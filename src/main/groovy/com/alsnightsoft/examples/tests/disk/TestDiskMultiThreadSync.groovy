package com.alsnightsoft.examples.tests.disk

import com.alsnightsoft.examples.models.DataLoader
import groovy.transform.CompileStatic

import java.util.concurrent.Executors
import java.util.concurrent.Future

/**
 * Case multi Threads
 *
 *  Created by aluis on 5/11/16.
 */
@CompileStatic
class TestDiskMultiThreadSync extends TestDiskBase {

    public TestDiskMultiThreadSync() {
        // Size size (one for 1,000 DataTest)
        executorService = Executors.newFixedThreadPool(data.size())
    }

    @Override
    public final void run() {
        startTime()
        for (final DataLoader dataLoader : data) {
            addThread2(dataLoader)
        }
        for (Future<Boolean> future : futures) {
            future.get()
        }
        println "Test Multi thread Complete sync (" + data.size() + ") same size of list"
        showFinishTime()
        showStatistics()
    }
}
