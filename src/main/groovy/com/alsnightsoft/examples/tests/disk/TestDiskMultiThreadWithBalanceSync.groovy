package com.alsnightsoft.examples.tests.disk

import com.alsnightsoft.examples.models.DataLoader
import groovy.transform.CompileStatic

import java.util.concurrent.Executors
import java.util.concurrent.Future

/**
 * Case multi Threads with balance data load
 * Control and good pool size.
 *
 *  Created by aluis on 5/11/16.
 */
@CompileStatic
class TestDiskMultiThreadWithBalanceSync extends TestDiskBase {

    public TestDiskMultiThreadWithBalanceSync() {
        executorService = Executors.newFixedThreadPool(threads)
    }

    @Override
    public final void run() {
        startTime()
        for (final DataLoader dataLoader : data) {
            addThread2(dataLoader)
        }
        for (Future<Boolean> future : futures) {
            future.get()
        }
        println "Test Multi thread Complete sync (" + threads + ") on data balance"
        showFinishTime()
        showStatistics()
    }
}
