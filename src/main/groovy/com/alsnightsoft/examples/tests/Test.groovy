package com.alsnightsoft.examples.tests

import com.alsnightsoft.examples.models.statistics.NumberFreq
import groovy.transform.CompileStatic

import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Future

/**
 *  Created by aluis on 5/12/16.
 */
@CompileStatic
abstract class Test {

    protected static final int threads = 20
    protected ExecutorService executorService
    protected List<Future<Boolean>> futures = new ArrayList<>()
    protected final synchronized Map<Long, NumberFreq> numberFreqs = new HashMap<>()

    protected long initTime

    public abstract void run()

    protected synchronized void showStatistics() {
        for (String key : numberFreqs.keySet()) {
            NumberFreq numberFreq = numberFreqs.get(key)
            if (numberFreq) {
                println "Number: " + numberFreq.getNumber() + " Repeat: " + numberFreq.getFreq()
            }
        }
        println ""
    }

    protected void startTime() {
        initTime = System.currentTimeMillis()
    }

    protected void showFinishTime() {
        println "Init Time: " + initTime + " MS"
        println "Finish time: " + System.currentTimeMillis() + " MS"
        println "Result time: " + (System.currentTimeMillis() - initTime) + " MS"
    }

    protected Future<Boolean> processData(Callable<Boolean> callable) {
        return executorService.submit(callable)
    }

    public void shutdown() {
        if (executorService) {
            executorService.shutdown()
        }
    }
}
