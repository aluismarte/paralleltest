package com.alsnightsoft.examples.tests.db

import com.alsnightsoft.examples.domains.GroupData
import com.alsnightsoft.examples.domains.NumbersData
import com.alsnightsoft.examples.models.statistics.NumberFreq
import com.alsnightsoft.examples.services.GroupDataService
import com.alsnightsoft.examples.tests.Test
import groovy.transform.CompileStatic

import java.util.concurrent.Callable
import java.util.concurrent.Future

/**
 *  Created by aluis on 5/12/16.
 */
@CompileStatic
abstract class TestDBBase extends Test {

    public static final int DATA_FIND = 50
    // Load only 1- cases of 50 assigned
    public static final int DATA_LOAD_FIND = 10

    public void process(NumbersData numbersData) {
        for (String split : numbersData.getData().split("-")) {
            sumFreq(split)
        }
    }

    private void sumFreq(String number) {
        NumberFreq numberFreq = numberFreqs.get(Long.parseLong(number))
        if (!numberFreq) {
            synchronized (numberFreqs) {
                numberFreqs.put(Long.parseLong(number), new NumberFreq(number))
                numberFreq = numberFreqs.get(Long.parseLong(number))
            }
        }
        numberFreq.sumFreq()
    }

    protected Future<Boolean> addThreadSimple(GroupDataService groupDataService, int typeData) {
        return processData(new Callable<Boolean>() {
            @Override
            Boolean call() throws Exception {
                int total = groupDataService.countGroupData(true, typeData)
                println "Total: " + total
                for (int i = 0; i < total; i += DATA_FIND) {
                    println "Start: " + i + " Find: " + DATA_FIND
                    for (GroupData groupData : groupDataService.listGroupData(true, i, DATA_FIND, typeData)) {
                        for (NumbersData numbersData : groupDataService.refresh(groupData.id).getListNumbers()) {
                            process(numbersData)
                        }
                    }
                }
                println ""
                return true
            }
        })
    }

    protected void addThread(GroupDataService groupDataService, int start, int size, int typeData) {
        futures.add(processData(new Callable<Boolean>() {
            @Override
            Boolean call() throws Exception {
                for (GroupData groupData : groupDataService.listGroupData(true, start, size, typeData)) {
                    for (NumbersData numbersData : groupDataService.refresh(groupData.id).getListNumbers()) {
                        process(numbersData)
                    }
                }
                return true
            }
        }))
    }

    protected void addThread(GroupDataService groupDataService, int start, int size, int load, int typeData) {
        futures.add(processData(new Callable<Boolean>() {
            @Override
            Boolean call() throws Exception {
                for (int i = 0; i < size; i += load) {
                    for (GroupData groupData : groupDataService.listGroupData(true, (start + i), load, typeData)) {
                        for (NumbersData numbersData : groupDataService.refresh(groupData.id).getListNumbers()) {
                            process(numbersData)
                        }
                    }
                }
                return true
            }
        }))
    }

}
