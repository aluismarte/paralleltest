package com.alsnightsoft.examples.tests.db

import com.alsnightsoft.examples.models.constants.TypeData
import com.alsnightsoft.examples.services.GroupDataService
import com.alsnightsoft.examples.utils.Constant
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

import java.util.concurrent.Executors
import java.util.concurrent.Future

/**
 *  Created by aluis on 5/12/16.
 */
@CompileStatic
class TestDBMultiThreadTwoDigits extends TestDBBase {

    @Autowired
    private GroupDataService groupDataService

    public TestDBMultiThreadTwoDigits() {
        Constant.instance.autoWiredClass(this)
        executorService = Executors.newFixedThreadPool(groupDataService.countGroupData(true, TypeData.TWO_DIGITS))
    }

    @Override
    public final void run() {
        startTime()
        int total = groupDataService.countGroupData(true, TypeData.TWO_DIGITS)
        println "Total: " + total
        for (int i = 0; i < total; i += DATA_FIND) {
            println "Start: " + i + " Find: " + DATA_FIND
            addThread(groupDataService, i, DATA_FIND, TypeData.TWO_DIGITS)
        }
        for (Future<Boolean> future : futures) {
            future.get()
        }
        println "Test Multi thread only necessary sync data with 2 digits (" + total + ") same size of list"
        showFinishTime()
        showStatistics()
    }
}
