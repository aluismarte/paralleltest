package com.alsnightsoft.examples.tests.db

import com.alsnightsoft.examples.models.constants.TypeData
import com.alsnightsoft.examples.services.GroupDataService
import com.alsnightsoft.examples.utils.Constant
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

import java.util.concurrent.Executors
import java.util.concurrent.Future

/**
 *  Created by aluis on 5/12/16.
 */
@CompileStatic
class TestDBMultiThreadBase extends TestDBBase {

    @Autowired
    private GroupDataService groupDataService

    public TestDBMultiThreadBase() {
        Constant.instance.autoWiredClass(this)
        executorService = Executors.newFixedThreadPool(groupDataService.countGroupData(true, TypeData.BASE))
    }

    @Override
    public final void run() {
        startTime()
        int total = groupDataService.countGroupData(true, TypeData.BASE)
        println "Total: " + total
        for (int i = 0; i < total; i += DATA_FIND) {
            println "Start: " + i + " Find: " + DATA_FIND
            addThread(groupDataService, i, DATA_FIND, TypeData.BASE)
        }
        for (Future<Boolean> future : futures) {
            future.get()
        }
        println "Test Multi thread only necessary sync with data simple (" + total + ") same size of list"
        showFinishTime()
        showStatistics()
    }
}
