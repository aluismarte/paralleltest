package com.alsnightsoft.examples.tests.db

import com.alsnightsoft.examples.models.constants.TypeData
import com.alsnightsoft.examples.services.GroupDataService
import com.alsnightsoft.examples.utils.Constant
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

import java.util.concurrent.Executors
import java.util.concurrent.Future

/**
 *  Created by aluis on 5/12/16.
 */
@CompileStatic
class TestDBMonoThreadBase extends TestDBBase {

    @Autowired
    private GroupDataService groupDataService

    public TestDBMonoThreadBase() {
        Constant.instance.autoWiredClass(this)
        executorService = Executors.newFixedThreadPool(1)
    }

    @Override
    public final void run() {
        startTime()
        Future<Boolean> future = addThreadSimple(groupDataService, TypeData.BASE)
        future.get() // Force finish
        println "Test one thread only necessary sync with data simple"
        showFinishTime()
        showStatistics()
    }
}
