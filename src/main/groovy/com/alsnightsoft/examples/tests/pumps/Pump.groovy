package com.alsnightsoft.examples.tests.pumps

import groovy.transform.CompileStatic

import java.util.concurrent.ExecutorService

/**
 *  Created by aluis on 5/12/16.
 */
@CompileStatic
abstract class Pump {

    protected ExecutorService executorService
}
