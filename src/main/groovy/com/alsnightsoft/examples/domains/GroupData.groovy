package com.alsnightsoft.examples.domains

import com.alsnightsoft.examples.utils.Constant
import grails.persistence.Entity

/**
 *  Created by aluis on 5/12/16.
 */
@SuppressWarnings("GroovyUnusedDeclaration")
@Entity
class GroupData {

    int typeData

    static hasMany = [listNumbers: NumbersData]

    boolean enabled = true
    String createdBy = Constant.ROOT
    String modifyBy = Constant.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        // Work normal case
//        listNumbers lazy: false
    }

    static mapping = {
        table "group_data"

        listNumbers lazy: false // No work
    }
}
