package com.alsnightsoft.examples.domains

import com.alsnightsoft.examples.utils.Constant
import grails.persistence.Entity

/**
 *  Created by aluis on 5/12/16.
 */
@SuppressWarnings("GroovyUnusedDeclaration")
@Entity
class NumbersData {

    String data

    boolean enabled = true
    String createdBy = Constant.ROOT
    String modifyBy = Constant.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
    }

    static mapping = {
        table "numbers_data"
    }
}
