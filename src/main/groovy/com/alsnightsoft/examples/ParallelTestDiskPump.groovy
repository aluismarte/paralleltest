package com.alsnightsoft.examples

import com.alsnightsoft.examples.utils.Constant
import groovy.transform.CompileStatic
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.transaction.annotation.EnableTransactionManagement

/**
 * On this test will check number repeats
 *
 *  Created by aluis on 5/11/16.
 */
@CompileStatic
@SpringBootApplication
@EnableTransactionManagement
class ParallelTestDiskPump {

    public static void main(String[] args) throws Exception {
        diskTest()
        System.exit(0)
    }

    public static void diskTest() {
        println "Disk Test"
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize()
        Constant.instance.applicationContext.close()
    }

}
