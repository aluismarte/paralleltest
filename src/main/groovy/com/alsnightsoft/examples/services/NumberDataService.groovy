package com.alsnightsoft.examples.services

import com.alsnightsoft.examples.domains.NumbersData
import com.alsnightsoft.examples.utils.Constant
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by aluis on 5/12/16.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("numberDataService")
@Transactional(rollbackFor = Exception.class)
class NumberDataService {

    public NumbersData createNumbersData(NumbersData numbersData) {
        if (numbersData != null && !numbersData.hasErrors()) {
            if (NumbersData.exists(numbersData.id)) {
                numbersData.lastUpdated = new Date()
                numbersData.modifyBy = Constant.ROOT
                return numbersData.merge(flush: true, failOnError: true)
            } else {
                numbersData.createdBy = Constant.ROOT
                numbersData.modifyBy = Constant.ROOT
                return numbersData.save(flush: true, failOnError: true)
            }
        }
        return null
    }

    public List<NumbersData> listNumbersData(boolean enab, int start, int size) {
        return NumbersData.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            if (enab) {
                eq "enabled", enab
            }
        }.findAll()
    }

    public int countNumbersData(boolean enab) {
        if (enab) {
            return NumbersData.countByEnabled(enab)
        }
        return NumbersData.count()
    }
}
