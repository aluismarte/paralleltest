package com.alsnightsoft.examples.services

import com.alsnightsoft.examples.domains.GroupData
import com.alsnightsoft.examples.utils.Constant
import org.hibernate.FetchMode
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by aluis on 5/12/16.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("groupDataService")
@Transactional(rollbackFor = Exception.class)
class GroupDataService {

    public GroupData createGroupData(GroupData groupData) {
        if (groupData != null && !groupData.hasErrors()) {
            if (GroupData.exists(groupData.id)) {
                groupData.lastUpdated = new Date()
                groupData.modifyBy = Constant.ROOT
                return groupData.merge(flush: true, failOnError: true)
            } else {
                groupData.createdBy = Constant.ROOT
                groupData.modifyBy = Constant.ROOT
                return groupData.save(flush: true, failOnError: true)
            }
        }
        return null
    }

    public List<GroupData> listGroupData(boolean enab, int start, int size, int typeData) {
        return GroupData.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            if (enab) {
                eq "enabled", enab
            }
            eq "typeData", typeData
            fetchMode "listNumbers", FetchMode.SELECT
        }.findAll()
    }

    /**
     * This case is better on apps.
     *
     * Only partial data travel.
     *
     * @param id
     * @return
     */
    public GroupData refresh(Long id) {
        GroupData groupData = GroupData.findById(id)
        groupData.listNumbers.size()
        return groupData
    }

    public int countGroupData(boolean enab) {
        if (enab) {
            return GroupData.countByEnabled(enab)
        }
        return GroupData.count()
    }

    public int countGroupData(boolean enab, int typeData) {
        if (enab) {
            return GroupData.countByTypeDataAndEnabled(typeData, enab)
        }
        return GroupData.countByTypeData(typeData)
    }
}
