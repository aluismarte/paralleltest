package com.alsnightsoft.examples

import com.alsnightsoft.examples.models.DataTest
import com.alsnightsoft.examples.utils.Constant
import com.alsnightsoft.examples.utils.Generator
import groovy.transform.CompileStatic

/**
 *  Created by aluis on 5/11/16.
 */
@CompileStatic
class GenerateDataFile {

    public static void main(String[] args) {
        Generator generator = new Generator()
        File file = new File(Constant.instance.dataFileName)
        PrintWriter writerTest =  new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true))))
        for (int i = 0; i < 400; i++) {
            List<DataTest> dataTests = generator.generateRandomList(1000)
            writerTest.println(generator.convertData(dataTests))
        }
        writerTest.flush()
    }
}
